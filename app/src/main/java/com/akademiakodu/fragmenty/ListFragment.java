package com.akademiakodu.fragmenty;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListFragment extends MainFragment {
    private static final List<String> ELEMENTS = Arrays.asList(
            "Bogdan", "Zdzisław", "Piotr",
            "Mietek", "Henryk", "Miłosz"
    );

    @BindView(R.id.list)
    protected RecyclerView mList;

    private NamesAdapter.OnItemClick mClickListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (!(context instanceof NamesAdapter.OnItemClick)) {
            throw new RuntimeException("ListFragment wymaga Activity implemetujacej" +
                    "interfejs OnItemClick");
        }
        mClickListener = (NamesAdapter.OnItemClick) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list, container, false);
        ButterKnife.bind(this, view);

        mList.setLayoutManager(new LinearLayoutManager(getContext()));
        mList.setAdapter(new NamesAdapter(ELEMENTS, mClickListener));

        return view;
    }
}
