package com.akademiakodu.fragmenty;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NamesAdapter extends RecyclerView.Adapter<NamesAdapter.NameViewHolder> {
    private List<String> mData;
    private OnItemClick mClickListener;

    public NamesAdapter(List<String> mData, OnItemClick mClickListener) {
        this.mData = mData;
        this.mClickListener = mClickListener;
    }

    @Override
    public NameViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(android.R.layout.simple_list_item_1, parent, false);
        return new NameViewHolder(view, mClickListener);
    }

    @Override
    public void onBindViewHolder(NameViewHolder holder, int position) {
        holder.mLabel.setText(mData.get(position));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class NameViewHolder extends RecyclerView.ViewHolder {
        @BindView(android.R.id.text1)
        protected TextView mLabel;
        private OnItemClick mClickListener;

        public NameViewHolder(View itemView, OnItemClick clickListener) {
            super(itemView);
            this.mClickListener = clickListener;
            ButterKnife.bind(this, itemView);
        }

        @OnClick
        protected void onClick() {
            if (mClickListener != null) {
                mClickListener.onItemClick(mLabel.getText().toString());
            }
        }
    }

    public interface OnItemClick {
        void onItemClick(String value);
    }
}
