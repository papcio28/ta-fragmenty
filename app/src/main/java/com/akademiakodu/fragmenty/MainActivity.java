package com.akademiakodu.fragmenty;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class MainActivity extends AppCompatActivity implements NamesAdapter.OnItemClick {
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_container, new ListFragment())
                    .commit();
        }

        Log.d(TAG, "onCreate");
    }

    @Override
    public void onItemClick(String value) {
        displayFragment(value);
    }

    private void displayFragment(String value) {
        if (isDetailsVisible()) {
            // Dla Landscape
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.details_container, DetailsFragment.newInstance(value), "x")
                    .commit();
        } else {
            DetailsActivity.start(this, value);
        }
    }

    private boolean isDetailsVisible() {
        return findViewById(R.id.details_container) != null;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d(TAG, "onRestoreInstanceState");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }
}
