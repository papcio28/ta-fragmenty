package com.akademiakodu.fragmenty;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class DetailsActivity extends AppCompatActivity {
    private static final String EXTRA_NAME = "name";

    public static void start(Context context, String name) {
        Intent intent = new Intent(context, DetailsActivity.class);
        intent.putExtra(EXTRA_NAME, name);

        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.details_container,
                        DetailsFragment.newInstance(getIntent().getStringExtra(EXTRA_NAME)))
                .commit();
    }
}
