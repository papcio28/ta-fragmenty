package com.akademiakodu.fragmenty;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailsFragment extends MainFragment {
    private static final String ARG_VALUE = "value";

    public static DetailsFragment newInstance(String value) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();

        args.putString(ARG_VALUE, value);
        fragment.setArguments(args);

        return fragment;
    }

    @BindView(R.id.details_text)
    protected TextView mDetailsText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details, container, false);
        ButterKnife.bind(this, view);

        mDetailsText.setText(getArguments().getString(ARG_VALUE));

        return view;
    }
}
